package proj2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class DoFlipkart {
	
	//public static void main(String[] args) throws InterruptedException {
	@Test
	public void doFlipkart() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		Thread.sleep(2000);
		WebElement eleloginClose = driver.findElementByXPath("(//button)[2]");
		eleloginClose.click();
		
		Thread.sleep(2000);
		WebElement eleElec = driver.findElementByXPath("//span[text()='Electronics']");
		Actions act = new Actions(driver);
		act.moveToElement(eleElec).perform();
		
		Thread.sleep(3000);
		WebElement eleMi = driver.findElementByXPath("(//li[@class='_1KCOnI _3ZgIXy']/a[@title='Mi'])[1]");
		eleMi.click();
		
		WebElement pageTitle = driver.findElementByXPath("//h1[text()='Mi Mobiles']");
		String txt = pageTitle.getText();
		if(txt.equalsIgnoreCase("Mi Mobiles"))
			System.out.println("It is on the correct page of Mi mobile...");
		
		Thread.sleep(3000);
		WebElement eleNew = driver.findElementByXPath("//div[text()='Newest First']");
		eleNew.click();
		
		Thread.sleep(2000);
		List<WebElement> Products = driver.findElementsByXPath("//*[@class='_3wU53n']");
		for (WebElement each : Products) {
			
			System.out.println("Product name is: "+each.getText());
			
		}
		List<WebElement> Prices = driver.findElementsByXPath("//*[@class='_1vC4OE _2rQ-NK']");
		for (WebElement each : Prices) {
			
			System.out.println("Product price is: "+each.getText());
		}
		
		Thread.sleep(2000);
		WebElement firstProduct = driver.findElementByXPath("(//*[@class='_3wU53n'])[1]");
		String prodText = firstProduct.getText();
		firstProduct.click();
		
		Set<String> allwindowHandles = driver.getWindowHandles();
		List<String> allhandles = new ArrayList<String>(allwindowHandles);
		driver.switchTo().window(allhandles.get(1));
		
		Thread.sleep(2000);
		String pgeTitle = driver.getTitle();
		if(pgeTitle.contains(prodText))
			System.out.println("Title is correct on the page of first product...");
		
		Thread.sleep(2000);
		//WebElement ratings = driver.findElementByXPath("(//*[@class='col-12-12'])[1]");
		//System.out.println("Ratings for this product is: "+ratings.getText());
		WebElement rate = driver.findElementByXPath("//span[text()[contains(.,'Ratings')]]");
		System.out.println("Ratings for this product is: "+rate.getText());
		
		
		/*WebElement reviews = driver.findElementByXPath("(//*[@class='col-12-12'])[2]");
		System.out.println("Reviews for this product is: "+reviews.getText());*/
		
		WebElement review = driver.findElementByXPath("//span[text()[contains(.,'Reviews')]]");
		System.out.println("Reviews for this product is: "+review.getText());
		
		driver.quit();
		
	}

}