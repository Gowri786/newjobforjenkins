package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	public String nameinTable;
	public String LeadidinTable;
		
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.NAME, using = "id") WebElement eleLeadId;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']") WebElement eleFLButton;
	@FindBy (how = How.XPATH, using = "//div[@class='x-grid3-body']/div/table/tbody/tr/td/div/a") WebElement eleFirstTableValue;
	@FindBy(how = How.XPATH, using = "//div[@class='x-grid3-body']/div/table/tbody/tr/td[3]/div/a") WebElement eleFirstNameinTable;
	@FindBy(how = How.XPATH, using = "//input[@name='id']/following::input[1]") WebElement elefname;
	@FindBy(how = How.NAME, using = "phoneCountryCode") WebElement eleCountryCode;
	@FindBy(how = How.NAME, using = "phoneAreaCode") WebElement eleAreaCode;
	@FindBy(how = How.NAME, using = "phoneNumber") WebElement elePhoneno;
	@FindBy(how = How.NAME, using = "emailAddress") WebElement eleEmail;
	@FindBy(how = How.LINK_TEXT, using = "Phone") WebElement elePhoneTab;
	@FindBy(how = How.LINK_TEXT, using = "Email") WebElement eleEmailTab;
	
	@Given("Enter LeadId in Find Leads as (.*)")
	public FindLeadsPage enterLeadId(String data)
	{
		type(eleLeadId, data);
		return this;
	}
	
	@Given("Capture LeadId of first resulting record in table")
	public FindLeadsPage captureleadidinFL()
	{
		LeadidinTable = getText(eleFirstTableValue);
		return this;
	}
	
	@Given("Enter first name on NameandId tab as (.*)")
	public FindLeadsPage enterFirstName(String data)
	{
		type(elefname, data);
		return this;
	}
	
	@Given("Capture firstname of first resulting record in table")
	public FindLeadsPage captureFirstNameinTable()
	{
		nameinTable = getText(eleFirstNameinTable);
		return this;
	}
	
	@Given("Click on Phone tab in FL")
	public FindLeadsPage clickPhoneTab()
	{
		click(elePhoneTab);
		return this;
	}
	
	@Given("Enter find leads country code as (.*)")
	public FindLeadsPage enterCounCode(String data)
	{
		eleCountryCode.clear();
		type(eleCountryCode, data);
		return this;
	}
	
	@Given("Enter find leads area code as (.*)")
	public FindLeadsPage enterAreaCo(String data)
	{
		type(eleAreaCode, data);
		return this;
	}
	
	@Given("Enter phonenumber on Phone tab as (.*)")
	public FindLeadsPage enterPhoneNumber(String data)
	{
		type(elePhoneno, data);
		return this;
	}
	
	@Given("Click on Email tab")
	public FindLeadsPage clickEmailTab()
	{
		click(eleEmailTab);
		return this;
	}
	
	@Given("Enter an emailid as (.*)")
	public FindLeadsPage enterEmaiId(String data)
	{
		type(eleEmail, data);
		return this;
	}
	
	@Given("Click on Find Leads button")
	public FindLeadsPage clickFLButton() throws InterruptedException
	{
		click(eleFLButton);
		Thread.sleep(3000);
		return this;
	}
	
	@Given("Click on first resulting data in table")
	public ViewLeadPage clickFirstTableValue() throws InterruptedException
	{
		click(eleFirstTableValue);
		Thread.sleep(2000);
		return new ViewLeadPage();
	}
	
	@Given("Click on first resulting data in table to merge")
	public MergeLeadPage clickSpecialFirstTableValue()
	{
		clickWithNoSnap(eleFirstTableValue);
		switchToWindow(0);
		return new MergeLeadPage();
	}

}
