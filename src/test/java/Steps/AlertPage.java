package Steps;

import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class AlertPage extends ProjectMethods {
	
	public AlertPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@Given("Click on Ok to accept alert")
	public ViewLeadPage okAlert()
	{
		acceptAlert();
		return new ViewLeadPage();
	}
	
	@Given("Click on Cancel to deny alert")
	public MergeLeadPage notOkAlert()
	{
		dismissAlert();
		return new MergeLeadPage();
	}

}
