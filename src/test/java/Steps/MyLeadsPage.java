package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	
	public MyLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (how = How.LINK_TEXT, using = "Create Lead") WebElement eleCreateLeadLink;
	@FindBy (how = How.LINK_TEXT, using = "Merge Leads") WebElement eleMergeLeadLink;
	@FindBy (how = How.LINK_TEXT, using = "Find Leads") WebElement eleFindLeadLink;
	
	@Given("Click Create Lead link")
	public CreateLeadPage clickCLLink()
	{
		click(eleCreateLeadLink);
		return new CreateLeadPage();
	}
	
	@Given("Click Merge Leads link")
	public MergeLeadPage clickMLLink()
	{
		click(eleMergeLeadLink);
		return new MergeLeadPage();
	}
	
	@Given("Click on Find Leads link")
	public FindLeadsPage clickFLLink()
	{
		click(eleFindLeadLink);
		return new FindLeadsPage();
		
	}
}
