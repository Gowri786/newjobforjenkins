package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {
	
	public DuplicateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_generalProfTitle") WebElement eleNewTitle;
	
	@Given("Enter new title as (.*)")
	public DuplicateLeadPage giveNewTitle(String data)
	{
		type(eleNewTitle, data);
		return this;
	}

}
