package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {
	
	public EditLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "updateLeadForm_companyName") WebElement eleNewCompName;
	@FindBy(how = How.XPATH, using = "//input[@value='Update']") WebElement eleUpdateButton;
	
	@Given("Enter new company name as (.*)")
	public EditLeadPage enterNewCompanyName(String data)
	{
		eleNewCompName.clear();
		type(eleNewCompName, data);
		return this;
	}
	
	@Given("Click on Update button")
	public ViewLeadPage clickUpdateButton()
	{
		click(eleUpdateButton);
		return new ViewLeadPage();
	}
}
