package Steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class HooksConcept extends SeMethods {
	
	@Before
	public void beforeSteps(Scenario sc)
	{
		startResult();
		startTestModule(sc.getName(), sc.getId());
		test = startTestCase(sc.getName());
		test.assignCategory("Functional");
		test.assignAuthor("Gowri");
		startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	@After
	public void afterSteps(Scenario sc)
	{
		closeAllBrowsers();
		endResult();
	}

}
