package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {
	
	public MergeLeadPage() 
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (how = How.XPATH, using = "(//form[@name='MergePartyForm']/table/tbody/tr/td/a)[1]") WebElement FromIcon;
	@FindBy (how = How.XPATH, using = "(//form[@name='MergePartyForm']/table/tbody/tr/td/a)[2]") WebElement ToIcon;
	@FindBy (how = How.LINK_TEXT, using = "Merge") WebElement eleMergeButton;
	
	@Given("Click the icon next to From Lead field")
	public FindLeadsPage clickFromIcon()
	{
		clickWithNoSnap(FromIcon);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	@Given("Click the icon next to To Lead field")
	public FindLeadsPage clickToIcon()
	{
		clickWithNoSnap(ToIcon);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	@Given("Click on Merge button")
	public AlertPage clickMergeButton()
	{
		click(eleMergeButton);
		return new AlertPage();
	}
	
	
}
