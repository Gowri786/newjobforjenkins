package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public String fnameinVLpage;
	
	public ViewLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "viewLead_firstName_sp") WebElement viewfirstName;
	@FindBy(how = How.XPATH, using = "//a[text()='Delete']") WebElement eleDelButton;
	@FindBy(how = How.XPATH, using = "//a[text()='Edit']") WebElement eleEdtButton;
	@FindBy(how = How.XPATH, using = "//a[text()='Duplicate Lead']") WebElement eleDupleadButton;
	
	@Given("Capture first name in the created record")
	public ViewLeadPage checkingFirstName()
	{
		fnameinVLpage = getText(viewfirstName);
		System.out.println("Name checked...");
		return this;
	}
	
	@Given("Click on Edit button")
	public EditLeadPage clickEditBut()
	{
		click(eleEdtButton);
		return new EditLeadPage();
	}
	
	@Given("Verify changed company name")
	public ViewLeadPage checknewCompName()
	{
		System.out.println("Company name changed and verified..");
		return this;
	}
	
	@Given("Click on Delete button")
	public MyLeadsPage clickDeleteBut()
	{
		click(eleDelButton);
		return new MyLeadsPage();
	}
	
	@Given("Click on Duplicate Lead button")
	public DuplicateLeadPage clickDuplicateBut()
	{
		click(eleDupleadButton);
		return new DuplicateLeadPage();
	}
}
