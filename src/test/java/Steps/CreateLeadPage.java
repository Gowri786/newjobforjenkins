package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_companyName") WebElement eleCompName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement elefirstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName") WebElement elelastName;
	@FindBy(how = How.ID, using = "createLeadForm_primaryPhoneCountryCode") WebElement eleCouncode;
	@FindBy(how = How.ID, using = "createLeadForm_primaryPhoneAreaCode") WebElement eleAreacode;
	@FindBy(how = How.ID, using = "createLeadForm_primaryPhoneNumber") WebElement elePhoneno;
	@FindBy(how = How.ID, using = "createLeadForm_primaryEmail") WebElement eleEmailid;	
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement eleCLButton;
	
	@Given("Enter company name as (.*)")
	public CreateLeadPage GiveNameofCompany(String data)
	{
		type(eleCompName, data);
		return this;
	}
	
	@Given("Enter first name as (.*)")
	public CreateLeadPage GiveFirstName(String data)
	{
		type(elefirstName, data);
		return this;
	}
	
	@Given("Enter last name as (.*)")
	public CreateLeadPage GiveLastName(String data)
	{
		type(elelastName, data);
		return this;
	}
	
	@Given("Enter country code as (.*)")
	public CreateLeadPage enterCountryCode(String data)
	{
		type(eleCouncode, data);
		return this;
	}
	
	@Given("Enter area code as (.*)")
	public CreateLeadPage enterAreaCode(String data)
	{
		type(eleAreacode, data);
		return this;
	}
	
	@Given("Enter phone number as (.*)")
	public CreateLeadPage enterPhoneNo(String data)
	{
		type(elePhoneno, data);
		return this;
	}
	
	@Given("Enter email address as (.*)")
	public CreateLeadPage enterEmail(String data)
	{
		type(eleEmailid, data);
		return this;
	}
	
	@Given("Click create lead button")
	public ViewLeadPage clickCLButton()
	{
		click(eleCLButton);
		return new ViewLeadPage();
		
	}
	
}
