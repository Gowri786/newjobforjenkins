Feature: Deleting record for a given Lead

Scenario Outline: Deleting successful with a found record
Given Enter username as <uname>
And Enter password as <pwd>
And Click the login button
And Click the CRMSFA link
And Click the Leads link
And Click on Find Leads link
And Click on Phone tab in FL
And Enter find leads country code as <ccode>
And Enter find leads area code as <acode>
And Enter phonenumber on Phone tab as <phoneno>
And Click on Find Leads button
And Capture LeadId of first resulting record in table
And Click on first resulting data in table
And Click on Delete button
And Click on Find Leads link
And Enter LeadId in Find Leads as <leadid>
When Click on Find Leads button 
#Then Verify result with/no records

Examples:
|uname|pwd|ccode|acode|phoneno|leadid|
|DemoSalesManager|crmsfa|91|605|0123456789|LeadidinTable|