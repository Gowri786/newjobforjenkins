Feature: Creating a lead in Leaftaps

#Background:
#Given Open the browser
#And Maximize the browser
#And Set the timeout
#And Launch the URL

Scenario Outline:: Positive creation1
And Enter username as <uname>
And Enter password as <pwd>
And Click the login button
And Click the CRMSFA link
And Click the Leads link
And Click Create Lead link
And Enter company name as <cname>
And Enter first name as <fname>
And Enter last name as <lname>
And Enter country code as <Ccode>
And Enter area code as <acode>
And Enter phone number as <phoneno>
And Enter email address as <emailid>
When Click create lead button
Then Capture first name in the created record
#Then Verify the created lead

Examples:
|uname|pwd|cname|fname|lname|Ccode|acode|phoneno|emailid|
|DemoSalesManager|crmsfa|Infosys|Sweetline|Priya|91|605|0123456789|testing@gmail.com|
|DemoCSR|crmsfa|Aspire|Angel|Simon|91|001|5698732541|oruemail@yahoo.com|
|DemoSalesManager|crmsfa|TNEB|Beaula|Rani|91|365|2365147954|asdf@twitter.com|
|DemoCSR|crmsfa|VelTEch|Ambika|Bhuvan|91|123|3695148755|lkjh@fb.com|

#Scenario: Positive Login1
#And Enter username as DemoCSR
#And Enter password as crmsfa
#And Click the login button
#Scenario: Positive creation
#Given Open the browser
#And Maximize the browser
#And Set the timeout
#And Launch the URL
#And Enter username as DemoSalesManager
#And Enter password as crmsfa
#And Click the login button
#And Click the CRMSFA link
#And Click the Leads link
#And Click Create Lead link
#And Enter company name as HCL
#And Enter first name as Gowri
#And Enter last name as Karthik
#When Click create lead button
#Then Verify the created lead