Feature: Merging two leads in Leaftaps

Scenario Outline:: Successful merging of two leads
And Enter username as <uname>
And Enter password as <pwd>
And Click the login button
And Click the CRMSFA link
And Click the Leads link
And Click Merge Leads link
And Click the icon next to From Lead field
And Enter LeadId in Find Leads as <fromleadid>
And Click on Find Leads button
And Click on first resulting data in table to merge
And Click the icon next to To Lead field
And Enter LeadId in Find Leads as <toleadid>
And Click on Find Leads button
And Click on first resulting data in table to merge
And Click on Merge button
And Click on Ok to accept alert
And Click on Find Leads link
And Enter LeadId in Find Leads as <fromleadid>
When Click on Find Leads button
#Then Verify that no records to display

Examples:
|uname|pwd|fromleadid|toleadid|fromleadid|
|DemoSalesManager|crmsfa|10472|10473|10472|



