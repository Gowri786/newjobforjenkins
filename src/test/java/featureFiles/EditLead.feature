Feature: Finding a record for a given Lead name and Editing it.

Scenario Outline: Editing successful with a found record
Given Enter username as <uname>
And Enter password as <pwd>
And Click the login button
And Click the CRMSFA link
And Click the Leads link
And Click on Find Leads link
And Enter first name on NameandId tab as <fname>
And Click on Find Leads button
And Click on first resulting data in table
And Click on Edit button
And Enter new company name as <compname>
When Click on Update button
Then Verify changed company name

Examples:
|uname|pwd|fname|compname|
|DemoSalesManager|crmsfa|Angel|Edho|
|DemoCSR|crmsfa|Sweetline|Ennavo|