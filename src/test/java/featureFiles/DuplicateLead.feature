Feature: Duplicating a record of a given Lead

Scenario Outline: Duplicating successfully with a found record
Given Enter username as <uname>
And Enter password as <pwd>
And Click the login button
And Click the CRMSFA link
And Click the Leads link
And Click on Find Leads link
And Click on Email tab
And Enter an emailid as <emailid>
And Click on Find Leads button
And Capture firstname of first resulting record in table
And Click on first resulting data in table
And Click on Duplicate Lead button
And Enter new title as <Duplead>
When Click create lead button
#Then Verify firstname is same as captured

Examples:
|uname|pwd|emailid|Duplead|
|DemoSalesManager|crmsfa|asdf@twitter.com|Duplicate Lead|